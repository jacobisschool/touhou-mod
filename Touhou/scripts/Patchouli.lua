local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local collision = require "necro.game.tile.Collision"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local spell = require "necro.game.spell.Spell"
local character = require "necro.game.character.Character"
local confusion = require "necro.game.character.Confusion"
local shield = require "necro.game.system.Shield"
local invincibility = require "necro.game.character.Invincibility"
local health = require "necro.game.character.Health"
local inventory = require "necro.game.item.Inventory"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
	spellcastCastElement = {},
	spellcastHealOnKill = {
		field.int8("amount", 1),
	},
	weaponElement = {},
	spellcastSwapSpell = {
		constant.int8("amount",7),
		constant.table("spells",{
			"Touhou_spellcastFire",
			"Touhou_spellcastWater",
			"Touhou_spellcastWood",
			"Touhou_spellcastMetal",
			"Touhou_spellcastEarth",
			"Touhou_spellcastSun",
			"Touhou_spellcastMoon",
		}),
		constant.table("names",{
			"FIRE",
			"WATER",
			"WOOD",
			"METAL",
			"EARTH",
			"SUN",
			"MOON",
		}),
	},
	entityElement = {
		field.int8("element",1),
		field.string("spell","Touhou_spellcastFire"),
		field.string("name","FIRE"),
	},
}


-- patchouli
customEntities.extend {
    name = "Patchouli",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Patchouli",
			initialEquipment = {
				items = {
					"Touhou_WeaponCrystal",
					"ShovelBasic",
					"Bomb",
					"Touhou_SpellElements",
					"Touhou_SpellSwap",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_patchouli_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_patchouli.png",
				focusX = 200,
				focusY = 0,
			},
			Touhou_entityElement = {},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_patchouli_head.png",
			},
		}
	}
}

-- swap
customEntities.extend {
    name = "SpellSwap",
	template = customEntities.template.item(),
	
	data = {
        flyaway = "Element Swap",
        hint = "Swap elements!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_swap.png",
			height = 24,
			width = 24,
        },
        spell = {
            castType = "Touhou_spellcastSpellSwap",
        },
        spellCooldownTime = {
            cooldown = 0,
        },
	}
}

-- spellcast swap
commonSpell.registerSpell("spellcastSpellSwap", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Swap Element",
        },
        Touhou_spellcastSwapSpell = {},
    })

-- swap event
event.spellcast.add(
    "SwapElement",
    {
        order = "offset",
		sequence = 1,
		filter = "Touhou_spellcastSwapSpell",
    },
    function (ev)
		if ev.caster:hasComponent("Touhou_entityElement") then
			ev.caster.Touhou_entityElement.element = ev.caster.Touhou_entityElement.element + 1
			if ev.caster.Touhou_entityElement.element > ev.entity.Touhou_spellcastSwapSpell.amount then
				ev.caster.Touhou_entityElement.element = 1
			end
			ev.caster.Touhou_entityElement.spell = ev.entity.Touhou_spellcastSwapSpell.spells[ev.caster.Touhou_entityElement.element]
			ev.caster.Touhou_entityElement.name = ev.entity.Touhou_spellcastSwapSpell.names[ev.caster.Touhou_entityElement.element]
			
			local barrage = inventory.getItemInSlot(ev.caster, "spell", 1)
			if barrage:hasComponent("spell") then
				if barrage.spell.castType == "Touhou_spellcastSpellElements" then
					barrage.spellCooldownTime.remainingTurns = 1
				end
			end
		end
    end
)

-- crystal
customEntities.extend {
    name = "WeaponCrystal",
    template = customEntities.template.item(),

    data = {
        flyaway = "Elemental Crystal",
        hint = "Strike foes with elements!",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/crystal_elements.png",
			width = 25,
			height = 24,
		},
        weapon = {
			damage = 1,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "dagger",
                passWalls = false,
                multiHit = false,
                tiles = {
					{
                        offset = {1, 0},
                    },
                },
            },
        },
		Touhou_weaponElement = {},
    },
}

-- crystal event
event.holderDealDamage.add(
    "ElementAttack",
    {
        order = "applyDamage",
		filter = "Touhou_weaponElement",
    },
    function (ev)
		if ev.holder:hasComponent("Touhou_entityElement") and ev.type == damage.Type.PHYSICAL then
			local element = ev.holder.Touhou_entityElement.name
			if element == "FIRE" then
				ev.damage = ev.damage + 1
			elseif element == "WATER" then
				freeze.inflict(ev.victim,2)
			elseif element == "WOOD" then
				if ev.victim.health.health - ev.damage  <= 0 then
					move.direction(ev.victim, ev.holder.facingDirection.direction, -2, move.Type.TELEPORT)
					move.direction(ev.holder, ev.holder.facingDirection.direction, 1, move.Type.SLIDE)
					invincibility.activate(ev.holder,1)
				end
			elseif element == "METAL" then
				ev.penetration = damage.Penetration.PIERCING
			elseif element == "EARTH" then
				ev.knockback = 1
			elseif element == "SUN" then
				confusion.inflict(ev.victim,4)
			elseif element == "MOON" then
				shield.grantBarrier(ev.holder,3)
				ev.damage = ev.damage - 1
			end
		end
    end
)

-- elements
customEntities.extend {
    name = "SpellElements",
	template = customEntities.template.item(),
	
	data = {
        flyaway = "Element Barrage",
        hint = "Attack your foes with the elements!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_element.png",
			height = 24,
			width = 24,
        },
        spell = {
            castType = "Touhou_spellcastSpellElements",
        },
        spellCooldownTime = {
            cooldown = 128,
        },
		spellUseCasterFacingDirection = {},
	}
}

-- spellcast elements
commonSpell.registerSpell("spellcastSpellElements", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Element Barrage",
        },
        Touhou_spellcastCastElement = {},
    })

-- elements event
event.spellcast.add(
    "Elements",
    {
        order = "offset",
		sequence = 1,
		filter = "Touhou_spellcastCastElement",
    },
    function (ev)
		if ev.caster:hasComponent("Touhou_entityElement") then
			local sp = spell.cast(ev.caster, ev.caster.Touhou_entityElement.spell, ev.caster.facingDirection.direction)
		end
    end
)

-- fire spell
commonSpell.registerSpell("spellcastFire", {
		spellcastSwipe = {
            coreTexture = "ext/spells/fire0.png",
			textures = {
				"ext/spells/fire0.png",
				"ext/spells/fire1.png",
				"ext/spells/fire2.png",
				"ext/spells/fire3.png",
				"ext/spells/fire4.png",
			},
            width = 24,
            height = 24,
            frameCount = 7,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastMultiTileSwipe = {},
		team = {},
		facingDirection = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        friendlyName = {
            name = "Fire",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
    })
	
-- water spell
commonSpell.registerSpell("spellcastWater", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_water.png",
			textures = {
				"mods/Touhou/gfx/swipe_water.png"
			},
            width = 48,
            height = 72,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
        spellcastCone = {
            distance = 2,
        },
        spellcastAutoDespawn = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        team = {},
        friendlyName = {
            name = "Water",
        },
		spellcastInflictDamage = {
			damage = 1,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
		spellcastInflictFreeze = {
			turns = 4,
		},
    })
	
-- wood spell
commonSpell.registerSpell("spellcastWood", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_wood.png",
			textures = {
				"mods/Touhou/gfx/swipe_empty.png",
				"mods/Touhou/gfx/swipe_wood.png",
			},
            width = 24,
            height = 24,
            frameCount = 7,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastMultiTileSwipe = {},
		team = {},
		facingDirection = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        friendlyName = {
            name = "Wood",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
			maxDistance = 2,
		},
		spellcastInflictDamage = {
			damage = 1,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
		-- spellcastDirectionalOffset = {
			-- distance = 1,
		-- },
		Touhou_spellcastHealOnKill = {},
    })
	
-- metal spell
commonSpell.registerSpell("spellcastMetal", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_metal.png",
			textures = {
				"mods/Touhou/gfx/swipe_metal.png"
			},
            width = 72,
            height = 72,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
			offsetX = -24,
        },
		spellcastRectangular = {
			offsetX = -1,
			offsetY = -1,
			width = 3,
			height = 3,
		},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Metal",
        },
		spellcastInflictDamage = {
			damage = 1,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PIERCING,
		},
    })
	
-- earth spell
commonSpell.registerSpell("spellcastEarth", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_eart.png",
			textures = {
				"mods/Touhou/gfx/swipe_empty.png",
				"mods/Touhou/gfx/swipe_earth.png",
			},
            width = 24,
            height = 24,
            frameCount = 7,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastMultiTileSwipe = {},
		team = {},
		facingDirection = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        friendlyName = {
            name = "Earth",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
			maxDistance = 2,
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PHASING,
		},
		-- spellcastDirectionalOffset = {
			-- distance = 1,
		-- },
		Touhou_spellcastMassiveKnockback = {},
    })

-- sun spell
commonSpell.registerSpell("spellcastSun", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Sun",
        },
		team = {},
		spellcastTargetCaster = {},
        spellcastModifySize = {
			size = 1,
			turns = 4,
		},
    })
	
-- moon spell
commonSpell.registerSpell("spellcastMoon", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Moon",
        },
		team = {},
		spellcastTargetCaster = {},
        spellcastGrantBarrier = {
			turns = 4,
		},
    })
	
-- heal on kill
event.spellcast.add(
    "healOnKill",
    {
        order = "offset",
		filter = "spellcastInflictDamage",
    },
    function (ev)
		if ev.entity:hasComponent("Touhou_spellcastHealOnKill") then
			for victim in spellTargeting.entityIterator(ev) do
				if attack.isAttackable(ev.caster, victim) then
					if victim:hasComponent("health") and ev.entity:hasComponent("Touhou_spellcastHealOnKill") then
						if victim.health.health - ev.entity.spellcastInflictDamage.damage <= 0 then
							health.heal({
								entity = ev.caster,
								health = ev.entity.Touhou_spellcastHealOnKill.amount,
							})
						end
					end
				end
			end
		end
    end
)

-- spell hud
local hud = require "necro.render.hud.HUD"
local ui = require "necro.render.UI"


local function drawElement(entity)
	hud.drawText {
		text = "Element: " .. entity.Touhou_entityElement.name,
		font = ui.Font.MEDIUM,
		element = "timer",
		offsetX = 0,
		offsetY = -20,
	}
end

event.renderPlayerHUD.add("renderSpellElement", "grooveChain", function (entity)
	if entity and entity:hasComponent("Touhou_entityElement") then
		drawElement(entity)
	end
end)
