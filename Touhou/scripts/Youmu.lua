local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
	spellcastSwapSword = {
		constant.string("firstSword","Touhou_WeaponRoukanken"),
		constant.string("secondSword","Touhou_WeaponHakurouken"),
	},
	spellcastDash = {
		constant.int8("maxDistance",5),
	},
	spellcastBackdash = {
		constant.int8("maxDistance",3),
	},
	swordFirst = {
		field.bool("active",true),
	},
	swordSecond = {
		field.bool("active",false),
	}
}

-- event.spellCast.add(
    -- "swap",
    -- {
        -- order = "offset",
        -- sequence = 1,
        -- filter = "Touhou_spellcastSwapSword",
    -- },
    -- function (ev)
        -- local firstSword = ev.entity.Touhou_spellcastSwapSword.firstSword
		-- local secondSword = ev.entity.Touhou_spellcastSwapSword.secondSword
		-- if ev.caster:hasComponent("Touhou_swordFirst") then
			-- if ev.caster.Touhou_swordFirst.active == true then
				-- inventory.add(object.spawn(secondSword, ev.caster.position.x, ev.caster.position.y), ev.caster)
				-- ev.caster.Touhou_swordFirst.active = false
				-- ev.caster.Touhou_swordSecond.active = true
				-- -- object.kill(ev.caster)
			-- elseif ev.caster.Touhou_swordSecond.active == true then
				-- inventory.add(object.spawn(firstSword, ev.caster.position.x, ev.caster.position.y), ev.caster)
				-- ev.caster.Touhou_swordFirst.active = true
				-- ev.caster.Touhou_swordSecond.active = false
			-- end
		-- end
		
    -- end
-- )

-- -- swap sword
-- customEntities.extend {
    -- name = "SpellSwap",
	-- template = customEntities.template.item(),
	
	-- data = {
        -- flyaway = "Swap Sword",
        -- hint = "Swap between 2 swords!",
        -- slot = "spell",
    -- }, 
	
	-- components = {
        -- isSpell = true,
        -- sprite = {
            -- texture = "mods/Touhou/gfx/spell_swap.png",
			-- height = 27,
			-- width = 26,
        -- },
        -- spell = {
            -- castType = "Touhou_spellcastSwap",
        -- },
        -- spellCooldownTime = {
            -- cooldown = 0,
        -- },
	-- }
-- }

-- -- spellcast swap
-- commonSpell.registerSpell("spellcastSwap", {
        -- spellcastProcessImmediately = {},
        -- soundSpellcast = {
            -- sound = "spellGeneral",
        -- },
        -- friendlyName = {
            -- name = "Swap Sword",
        -- },
        -- Touhou_spellcastSwapSword = {},
    -- })


-- youmu
customEntities.extend {
    name = "Youmu",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Youmu",
			initialEquipment = {
				items = {
					"Touhou_WeaponRoukanken",
					"Touhou_WeaponHakurouken",
					"ShovelBasic",
					"Bomb",
					-- "Touhou_SpellSwap",
					"Touhou_SpellDash",
					"Touhou_SpellBackdash",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_youmu_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_youmu.png",
				focusX = 200,
				focusY = 200,
			},
			Touhou_swordFirst = {
				active = true
			},
			Touhou_swordSecond = {
				active = false
			},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_youmu_head.png",
			},
		}
	}
}

-- roukanken
customEntities.extend {
    name = "WeaponRoukanken",
    template = customEntities.template.item(),

    data = {
        flyaway = "Roukanken",
        hint = "Wide attack!",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/roukanken.png",
			width = 26,
			height = 27,
		},
        weapon = {
			damage = 1,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
                swipe = "broadsword",
                passWalls = true,
                multiHit = true,
                tiles = {
					{
                        offset = {1, -1},
						clearance = {{1, 0}},
                    },
					{
                        offset = {1, 1},
						clearance = {{1, 0}},
                    },
                },
            },
        },

        itemConditionalInvincibility = {},
        itemConditionalInvincibilityOnKill = {},
        itemDashOnKill = {
            moveType = move.Type.SLIDE,
        },

    },
}

-- hakurouken
customEntities.extend {
    name = "WeaponHakurouken",
    template = customEntities.template.item(),

    data = {
        flyaway = "Hakurouken",
        hint = "3 Damage!",
        slot = "misc",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/hakurouken.png",
			width = 26,
			height = 27,
		},
        weapon = {
			damage = 3,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
                swipe = "dagger",
                passWalls = false,
                multiHit = true,
                tiles = {
                    {
                        offset = {1, 0},
                    },
                },
            },
        },
    },
}

-- dash
customEntities.extend {
    name = "SpellDash",
	template = customEntities.template.item(),
	
	data = {
        flyaway = "Dash",
        hint = "Dash forwards very fast!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_dash.png",
			height = 24,
			width = 24,
        },
        spell = {
            castType = "Touhou_spellcastSpellDash",
        },
        spellCooldownTime = {
            cooldown = 0,
        },
	}
}

-- spellcast dash
commonSpell.registerSpell("spellcastSpellDash", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Dash",
        },
        Touhou_spellcastDash = {},
    })

-- dash
event.spellcast.add(
    "Dash",
    {
        order = "offset",
		filter = "Touhou_spellcastDash",
    },
    function (ev)
		for i = 1,ev.entity.Touhou_spellcastDash.maxDistance,1 do
			move.direction(ev.caster, ev.caster.facingDirection.direction, 1, move.Type.SLIDE)
		end
    end
)

-- backdash
customEntities.extend {
    name = "SpellBackdash",
	template = customEntities.template.item(),
	
	data = {
        flyaway = "Dash",
        hint = "Dash backwards very fast!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_backdash.png",
			height = 24,
			width = 24,
        },
        spell = {
            castType = "Touhou_spellcastSpellBackdash",
        },
        spellCooldownTime = {
            cooldown = 0,
        },
	}
}

-- spellcast dash
commonSpell.registerSpell("spellcastSpellBackdash", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Backdash",
        },
        Touhou_spellcastBackdash = {},
    })

-- backdash
event.spellcast.add(
    "Backdash",
    {
        order = "offset",
		sequence = 1,
		filter = "Touhou_spellcastBackdash",
    },
    function (ev)
		for i = 1,ev.entity.Touhou_spellcastBackdash.maxDistance,1 do
			move.direction(ev.caster, ev.caster.facingDirection.direction, -1, move.Type.SLIDE)
		end
    end
)