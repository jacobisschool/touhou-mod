local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local collision = require "necro.game.tile.Collision"
local action = require "necro.game.system.Action"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local team = require "necro.game.character.Team"
local character = require "necro.game.character.Character"
local inventory = require "necro.game.item.Inventory"
local damage = require "necro.game.system.Damage"
local move = require "necro.game.system.Move"
local timer = require "system.utils.Timer"
local turn = require "necro.cycles.Turn"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
	spellcastStopwatch = {
		field.int32("duration",10000),
	},
}

-- sakuya
customEntities.extend {
    name = "Sakuya",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Sakuya",
			initialEquipment = {
				items = {
					"WeaponEli",
					"ShovelBasic",
					"Touhou_SpellThrow",
					"Touhou_SpellFan",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_sakuya_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_sakuya.png",
				focusX = 290,
				focusY = 180,
			},
		}, {
			sprite = {
				texture = "mods/Touhou/gfx/char_sakuya_head.png",
			},
		}
	}
}

-- fan
customEntities.extend {
        name = "SpellFan",
		template = customEntities.template.item(),
		
		data = {
			flyaway = "Knife Fan",
			hint = "Throws a fan of knives",
			slot = "spell",
		},
		
		components = {
			isSpell = true,
			sprite = {
				texture = "mods/Touhou/gfx/spell_fan.png",
			},
			spell = {
				castType = "Touhou_spellcastFan",
			},
			spellCooldownTime = {
				cooldown = 64,
			},
			spellUseCasterFacingDirection = {},
		},
}

-- fan spell
commonSpell.registerSpell("spellcastFan", {
		spellcastSwipe = {
            textures = {
				"mods/Touhou/gfx/swipe_fan.png"
			},
            width = 48,
            height = 72,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
        spellcastCone = {
            distance = 2,
        },
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Knife Fan",
        },
		spellcastInflictDamage = {
			damage = 5,
			type = damage.Type.PHYSICAL,
			penetration = damage.Penetration.PIERCING,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
		Touhou_spellcastStopwatch = {},
    })
	
event.spellcast.add(
    "Stopwatch",
    {
        order = "offset",
		sequence = 1,
		filter = "Touhou_spellcastStopwatch",
    },
	function(ev)
		if ev.caster:hasComponent("rhythmIgnoredTemporarily") then
			local time = timer.milliseconds(ev.entity.Touhou_spellcastStopwatch.duration)
			local targetTurnID = turn.getCurrentTurnID() + turn.getTurnIDForTime(timer.toSeconds(time))
			if ev.caster.rhythmIgnoredTemporarily.endTurnID < turn.getCurrentTurnID()  then
				ev.caster.rhythmIgnoredTemporarily.endTurnID = math.max(ev.caster.rhythmIgnoredTemporarily.endTurnID, targetTurnID)
			end
		end
	end
)

-- throw
customEntities.extend {
    name = "SpellThrow",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Knife Throw",
        hint = "Throws a knife",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_throw.png",
        },
        spell = {
            castType = "Touhou_spellcastThrow",
        },
        spellCooldownTime = {
            cooldown = 0,
        },
		spellUseCasterFacingDirection = {},
    }
}

-- throw spell
commonSpell.registerSpell("spellcastThrow", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_throw.png",
			textures = {
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
				"mods/Touhou/gfx/swipe_throw.png",
			},
            width = 24,
            height = 24,
            frameCount = 3,
            duration = 150,
            diagonalScale = math.sqrt(2),
        },
		spellcastMultiTileSwipe = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Knife Throw",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.PHYSICAL,
			penetration = damage.Penetration.NONE,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
    })