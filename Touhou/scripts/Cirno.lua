local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local collision = require "necro.game.tile.Collision"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
}


-- cirno
customEntities.extend {
    name = "Cirno",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Cirno",
			initialEquipment = {
				items = {
					"Touhou_WeaponIcicle",
					"ShovelBasic",
					"Bomb",
					"Touhou_SpellPerfectFreeze",
					"Touhou_SpellIcicleBeam",
					"FeetBootsWinged",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_cirno_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_cirno.png",
				focusX = 350,
				focusY = 200,
			},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_cirno_head.png",
			},
		}
	}
}

-- icicle
customEntities.extend {
    name = "WeaponIcicle",
    template = customEntities.template.item(),

    data = {
        flyaway = "Icicle",
        hint = "Freeze enemies!",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/icicle.png",
			width = 26,
			height = 27,
		},
        weapon = {
			damage = 2,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "rapier",
                passWalls = false,
                multiHit = true,
                tiles = {
					{
                        offset = {1, 0},
                    },
					{
                        offset = {2, 0},
						targetFlags = attack.Flag.NONE,
                    },
                },
            },
        },
		itemFreezeOnAttack = {
			duration = 2,
		},
    },
}

-- icicle beam
customEntities.extend {
    name = "SpellIcicleBeam",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Icicle Beam",
        hint = "Shoots a beam of icicles!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_iciclebeam.png",
        },
        spell = {
            castType = "Touhou_spellcastIcicleBeam",
        },
        spellCooldownTime = {
            cooldown = 4,
        },
		spellUseCasterFacingDirection = {},
    }
}

-- icicle beam spell
commonSpell.registerSpell("spellcastIcicleBeam", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_iciclebeam.png",
			textures = {
				"mods/Touhou/gfx/swipe_iciclebeam.png",
			},
            width = 72,
            height = 24,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
			maxDistance = 3,
		},
        spellcastAutoDespawn = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        team = {},
        friendlyName = {
            name = "Icicle beam",
        },
		spellcastInflictDamage = {
			damage = 1,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PHASING,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
		spellcastInflictFreeze = {
			turns = 4,
		},
    })

-- perfect freeze
customEntities.extend {
        name = "SpellPerfectFreeze",
		template = customEntities.template.item(),
		
		data = {
			flyaway = "Perfect Freeze",
			hint = "Shoots the Perfect Freeze!",
			slot = "spell",
		},
		
		components = {
			isSpell = true,
			sprite = {
				texture = "mods/Touhou/gfx/spell_perfectfreeze.png",
			},
			spell = {
				castType = "Touhou_spellcastPerfectFreeze",
			},
			spellCooldownTime = {
				cooldown = 16,
			},
			spellUseCasterFacingDirection = {},
		},
}

-- perfect freeze spell
commonSpell.registerSpell("spellcastPerfectFreeze", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_perfectfreeze.png",
			textures = {
				"mods/Touhou/gfx/swipe_perfectfreeze.png"
			},
            width = 144,
            height = 216,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
        spellcastCone = {
            distance = 5,
        },
        spellcastAutoDespawn = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        team = {},
        friendlyName = {
            name = "Perfect Freeze",
        },
		spellcastInflictDamage = {
			damage = 1,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PHASING,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
		spellcastInflictFreeze = {
			turns = 8,
		},
    })