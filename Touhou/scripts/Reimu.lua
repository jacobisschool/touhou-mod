local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local collision = require "necro.game.tile.Collision"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local spell = require "necro.game.spell.Spell"
local health = require "necro.game.character.Health"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
}


-- reimu
customEntities.extend {
    name = "Reimu",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Reimu",
			initialEquipment = {
				items = {
					"Touhou_WeaponPaper",
					"ShovelBasic",
					"Bomb",
					"Touhou_SpellAmulet",
					"Touhou_SpellFantasy",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_reimu_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_reimu.png",
				focusX = 200,
				focusY = 0,
			},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_reimu_head.png",
			},
		}
	}
}

-- paper
customEntities.extend {
    name = "WeaponPaper",
    template = customEntities.template.item(),

    data = {
        flyaway = "Paper",
        hint = "Jab at enemies!",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/paper.png",
			width = 26,
			height = 26,
		},
        weapon = {
			damage = 1,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "rapier",
                passWalls = true,
				multihit = true,
                tiles = {
					{
                        offset = {1, 0},
                    },
					{
                        offset = {2, 0},
						clearance = {{1, 0}},
                    },
                },
            },
        },
		weaponKnockback = {
			distance = 1,
		},

    },
}

-- amulet
customEntities.extend {
    name = "SpellAmulet",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Amulet",
        hint = "Shoots amulets!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_amulet.png",
        },
        spell = {
            castType = "Touhou_spellcastAmulet",
        },
        spellCooldownTime = {
            cooldown = 0,
        },
		spellUseCasterFacingDirection = {},
    }
}

-- amulet spell
commonSpell.registerSpell("spellcastAmulet", {
		spellcastSwipe = {
			coreTexture = "mods/Touhou/gfx/swipe_empty.png",
			textures = {
				"mods/Touhou/gfx/swipe_a1.png",
				"mods/Touhou/gfx/swipe_a2.png",
				"mods/Touhou/gfx/swipe_a3.png",
			},
			width = 24,
            height = 24,
            frameCount = 7,
            duration = 140,
			diagonalScale = math.sqrt(2),
		},
		spellcastMultiTileSwipe = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Amulet",
        },
        soundSpellcast = {
            sound = "spellGeneral",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
			maxDistance = 3,
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
		spellcastMulticast = {
			castCount = 4,
			rotation = 3,
		},
    })
	
-- amulet 2 spell
commonSpell.registerSpell("spellcastAmuletB", {
		spellcastSwipe = {
			coreTexture = "mods/Touhou/gfx/swipe_empty.png",
			textures = {
				"mods/Touhou/gfx/swipe_a1.png",
				"mods/Touhou/gfx/swipe_a2.png",
				"mods/Touhou/gfx/swipe_a3.png",
			},
			width = 24,
            height = 24,
            frameCount = 7,
            duration = 140,
			diagonalScale = math.sqrt(2),
		},
		spellcastMultiTileSwipe = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Amulet",
        },
        soundSpellcast = {
            sound = "spellGeneral",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
			maxDistance = 3,
		},
		spellcastDirectionalOffset = {
			distance = 3,
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
		spellcastMulticast = {
			castCount = 8,
			rotation = 2,
		},
    })
	
-- fantasy seal
customEntities.extend {
    name = "SpellFantasy",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Fantast Seal",
        hint = "Shoots the fantasy seal!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_fantasy.png",
        },
        spell = {
            castType = "Touhou_spellcastFantasy",
        },
        spellCooldownTime = {
            cooldown = 24,
        },
		spellUseCasterFacingDirection = {},
    }
}
	
-- fantasy spell
commonSpell.registerSpell("spellcastFantasy", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_fantasy.png",
			textures = {
				"mods/Touhou/gfx/swipe_fantasy.png"
			},
            width = 120,
            height = 120,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
			offsetX = -48,
        },
		spellcastRectangular = {
			offsetX = -2,
			offsetY = -2,
			width = 5,
			height = 5,
		},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Fantasy Seal",
        },
		spellcastInflictDamage = {
			damage = 3,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PIERCING,
		},
		Touhou_spellcastCastSpell = {
			spell = "Touhou_spellcastAmuletB"
		},
    })