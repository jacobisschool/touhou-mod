local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local collision = require "necro.game.tile.Collision"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local spell = require "necro.game.spell.Spell"
local character = require "necro.game.character.Character"
local confusion = require "necro.game.character.Confusion"
local shield = require "necro.game.system.Shield"
local invincibility = require "necro.game.character.Invincibility"
local health = require "necro.game.character.Health"
local inventory = require "necro.game.item.Inventory"
local swipe = require "necro.game.system.Swipe"

event.swipe.add("ckick", "ckick", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_ckick.png"
    ev.entity.swipe.frameCount = 3
    ev.entity.swipe.width = 24
    ev.entity.swipe.height = 24
    ev.entity.swipe.duration = 200
end)

event.swipe.add("cslash", "cslash", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_cslash.png"
    ev.entity.swipe.frameCount = 3
    ev.entity.swipe.width = 24
    ev.entity.swipe.height = 72
    ev.entity.swipe.duration = 200
end)

event.swipe.add("clunge", "clunge", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_clunge.png"
    ev.entity.swipe.frameCount = 3
    ev.entity.swipe.width = 48
    ev.entity.swipe.height = 24
    ev.entity.swipe.duration = 200
end)

event.swipe.add("cslam", "cslam", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_cslam.png"
    ev.entity.swipe.frameCount = 3
    ev.entity.swipe.width = 48
    ev.entity.swipe.height = 72
    ev.entity.swipe.duration = 200
end)

event.swipe.add("cspin", "cspin", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_cspin.png"
    ev.entity.swipe.frameCount = 3
    ev.entity.swipe.width = 72
    ev.entity.swipe.height = 72
    ev.entity.swipe.duration = 200
	ev.entity.swipe.offsetX = -48
end)

event.swipe.add("cburst", "cburst", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_cburst.png"
    ev.entity.swipe.frameCount = 3
    ev.entity.swipe.width = 72
    ev.entity.swipe.height = 72
    ev.entity.swipe.duration = 200
end)

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
	spellcastWeaponCombo = {
		field.string("weapon","Touhou_WeaponKick")
	},
	weaponCombo = {
		field.string("weapon","Touhou_WeaponKick")
	},
	spellcastCastSpell = {
		field.string("spell","Touhou_spellcastExplosion")
	},
	spellcastCastSpellLate = {
		field.string("spell","Touhou_spellcastExplosion")
	},
	spellcastRectangular = {
		constant.int8("offsetX",0),
		constant.int8("offsetY",0),
		constant.int8("width",0),
		constant.int8("height",0),
	},
}


-- mokou
customEntities.extend {
    name = "Mokou",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Fujiwara no Mokou",
			initialEquipment = {
				items = {
					"Touhou_WeaponKick",
					"ShovelBasic",
					"Bomb",
					"Touhou_SpellPhoenix",
					"Touhou_SpellExplosion",
				},
			},
			health = {
				maxHealth = 10,
				health = 10,
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_mokou_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_mokou.png",
				focusX = 190,
				focusY = 0,
			},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_mokou_head.png",
			},
		}
	}
}

-- kick
customEntities.extend {
    name = "WeaponKick",
    template = customEntities.template.item(),

    data = {
        flyaway = "",
        hint = "",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/combo_kick.png",
			width = 24,
			height = 24,
		},
        weapon = {
			damage = 2,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "ckick",
                passWalls = false,
                multiHit = false,
                tiles = {
					{
                        offset = {1, 0},
                    },
                },
            },
        },
		Touhou_weaponCombo = {
			weapon = "Touhou_WeaponSlash",
		},
    },
}

-- slash
customEntities.extend {
    name = "WeaponSlash",
    template = customEntities.template.item(),

    data = {
        flyaway = "",
        hint = "",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/combo_slash.png",
			width = 24,
			height = 24,
		},
        weapon = {
			damage = 1,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "cslash",
                passWalls = true,
                multiHit = true,
                tiles = {
					{
                        offset = {1, -1},
						clearance = {{1, 0}},
                    },
					{
                        offset = {1, 0},
                    },
					{
                        offset = {1, 1},
						clearance = {{1, 0}},
                    },
                },
            },
        },
		Touhou_weaponCombo = {
			weapon = "Touhou_WeaponLunge",
		},
    },
}

-- lunge
customEntities.extend {
    name = "WeaponLunge",
    template = customEntities.template.item(),

    data = {
        flyaway = "",
        hint = "",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/combo_lunge.png",
			width = 24,
			height = 24,
		},
        weapon = {
			damage = 2,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "clunge",
                passWalls = true,
                multiHit = true,
                tiles = {
					{
                        offset = {1, 0},
                    },
					{
                        offset = {2, 0},
						clearance = {{1, 0}},
                    },
                },
            },
        },
		Touhou_weaponCombo = {
			weapon = "Touhou_WeaponSlam",
		},
    },
}

-- slam
customEntities.extend {
    name = "WeaponSlam",
    template = customEntities.template.item(),

    data = {
        flyaway = "",
        hint = "",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/combo_slam.png",
			width = 24,
			height = 24,
		},
        weapon = {
			damage = 1,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "cslam",
                passWalls = true,
                multiHit = true,
                tiles = {
					{
                        offset = {1, 0},
                    },
					{
                        offset = {1, -1},
						clearance = {{1, 0}},
                    },
					{
                        offset = {1, 1},
						clearance = {{1, 0}},
                    },
					{
                        offset = {2, 0},
						clearance = {{1, 0}},
                    },
					{
                        offset = {2, -1},
						clearance = {{1, -1}},
                    },
					{
                        offset = {2, 1},
						clearance = {{1, 1}},
                    },
                },
            },
        },
		Touhou_weaponCombo = {
			weapon = "Touhou_WeaponKick",
		},
    },
}

-- spin
customEntities.extend {
    name = "WeaponSpin",
    template = customEntities.template.item(),

    data = {
        flyaway = "",
        hint = "",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/combo_spin.png",
			width = 24,
			height = 24,
		},
        weapon = {
			damage = 2,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "cspin",
                passWalls = true,
                multiHit = true,
                tiles = {
					{
                        offset = {-1, 1},
                    },
					{
                        offset = {-1, 0},
                    },
					{
                        offset = {-1, -1},
                    },
					{
                        offset = {0, -1},
                    },
					{
                        offset = {1, -1},
                    },
					{
                        offset = {1, 0},
                    },
					{
                        offset = {1, 1},
                    },
					{
                        offset = {0, 1},
                    },
                },
            },
        },
		Touhou_weaponCombo = {
			weapon = "Touhou_WeaponLunge",
		},
    },
}
-- burst
customEntities.extend {
    name = "WeaponBurst",
    template = customEntities.template.item(),

    data = {
        flyaway = "",
        hint = "",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/combo_burst.png",
			width = 24,
			height = 24,
		},
        weapon = {
			damage = 2,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "cburst",
                passWalls = true,
                multiHit = true,
                tiles = {
					{
						offset = {1, 1},
					},
					{
						offset = {1, -1},
					},
					{
						offset = {1, 0},
					},
					{
						offset = {2, 1},
						clearance = {{1, 1}},
					},
					{
						offset = {2, -1},
						clearance = {{1, -1}},
					},
					{
						offset = {2, 0},
						clearance = {{1, 0}},
					},
					{
						offset = {3, 1},
						clearance = {{1, 1}, {2, 1}},
					},
					{
						offset = {3, -1},
						clearance = {{1, -1}, {2, -1}},
					},
					{
						offset = {3, 0},
						clearance = {{1, 0}, {2, 0}},
					},
				},
            },
        },
		Touhou_weaponCombo = {
			weapon = "Touhou_WeaponSlash",
		},
    },
}


-- combo weapon event
event.weaponAttack.add(
    "WeaponCombo",
    {
        order = "sound",
		sequence = 1,
		filter = "Touhou_weaponCombo",
    },
    function (ev)
		if #ev.result.targets > 0 then
			inventory.add(object.spawn(ev.weapon.Touhou_weaponCombo.weapon, ev.attacker.position.x, ev.attacker.position.y),ev.attacker)
			object.kill(ev.weapon)
		end
    end
)

-- combo spellcast event
event.spellcast.add(
    "SpellcastWeaponCombo",
    {
        order = "offset",
		filter = "Touhou_spellcastWeaponCombo",
    },
    function (ev)
		local wep = inventory.getItemInSlot(ev.caster, "weapon", 1)
		inventory.add(object.spawn(ev.entity.Touhou_spellcastWeaponCombo.weapon, ev.caster.position.x, ev.caster.position.y),ev.caster)
		object.kill(wep)
    end
)

-- explosion
customEntities.extend {
    name = "SpellExplosion",
	template = customEntities.template.item(),
	
	data = {
        flyaway = "Explosion",
        hint = "Dash backwards and shoot an explosion!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_explosion.png",
			height = 24,
			width = 24,
        },
        spell = {
            castType = "Touhou_spellcastExplode",
        },
        spellCooldownTime = {
            cooldown = 32,
        },
	}
}

-- spellcast explosion
commonSpell.registerSpell("spellcastExplode", {
        spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Backdash",
        },
        Touhou_spellcastBackdash = {},
		Touhou_spellcastWeaponCombo = {
			weapon = "Touhou_WeaponBurst",
		},
		Touhou_spellcastCastSpell = {
			spell = "Touhou_spellcastExplosion"
		},
    })

-- explosion
commonSpell.registerSpell("spellcastExplosion", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_explosion.png",
			textures = {
				"mods/Touhou/gfx/swipe_explosion.png"
			},
            width = 72,
            height = 72,
            frameCount = 3,
            duration = 200,
            diagonalScale = math.sqrt(2),
			offsetX = 24,
        },
		Touhou_spellcastRectangular = {
			offsetX = 1,
			offsetY = -1,
			width = 3,
			height = 3,
		},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Explosion",
        },
		spellcastInflictDamage = {
			damage = 3,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PIERCING,
		},
    })

-- spellcast on spellcast
event.spellcast.add(
    "SpellcastSpellCast",
    {
        order = "offset",
		filter = "Touhou_spellcastCastSpell",
    },
    function (ev)
		spell.cast(ev.caster,ev.entity.Touhou_spellcastCastSpell.spell, ev.caster.facingDirection.direction)
    end
)

-- spellcast on spellcast
event.spellcast.add(
    "SpellcastSpellCastLate",
    {
        order = "offset",
		sequence = 1,
		filter = "Touhou_spellcastCastSpellLate",
    },
    function (ev)
		spell.cast(ev.caster,ev.entity.Touhou_spellcastCastSpellLate.spell, ev.caster.facingDirection.direction)
    end
)

-- phoenix
customEntities.extend {
    name = "SpellPhoenix",
	template = customEntities.template.item(),
	
	data = {
        flyaway = "Explosion",
        hint = "Dash forwards and shoot a phoenix!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_phoenix.png",
			height = 24,
			width = 24,
        },
        spell = {
            castType = "Touhou_spellcastPhoenix",
        },
        spellCooldownTime = {
            cooldown = 32,
        },
	}
}

-- phoenix spellcast
commonSpell.registerSpell("spellcastPhoenix", {
		spellcastProcessImmediately = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Phoenix Dash",
        },
        Touhou_spellcastDash = {},
		Touhou_spellcastWeaponCombo = {
			weapon = "Touhou_WeaponSpin",
		},
		Touhou_spellcastCastSpell = {
			spell = "Touhou_spellcastPhoenixB"
		},
    })
	
-- phoenix spellcast
commonSpell.registerSpell("spellcastPhoenixB", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_phoenix.png",
			textures = {
				"mods/Touhou/gfx/swipe_phoenix.png"
			},
            width = 48,
            height = 72,
            frameCount = 3,
            duration = 200,
            diagonalScale = math.sqrt(2),
			offsetX = 24,
        },
		Touhou_spellcastRectangular = {
			offsetX = 1,
			offsetY = -1,
			width = 2,
			height = 3,
		},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Phoenix",
        },
		spellcastInflictDamage = {
			damage = 3,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PIERCING,
		},
    })
	
event.spellTargetArea.add("touhouRectangularTarget", {order = "rectangular", filter = "Touhou_spellcastRectangular"}, function (ev)
	local offsetX, offsetY = action.getMovementOffset(ev.caster.facingDirection.direction)
    local rect = ev.entity.Touhou_spellcastRectangular
	local width, height = rect.width, rect.height
	if not(offsetY == 0) then
		local tmp = width
		width = height
		height = tmp
		if offsetY < 0 then
			offsetY = height * -1
		end
		offsetX = math.floor(width/2) * -1
	else
		if offsetX < 0 then
			offsetX = width * -1
		end
		offsetY = math.floor(height/2) * -1
	end
    local x, y = ev.x + offsetX, ev.y + offsetY
    ev.areas[#ev.areas + 1] = {x = x, y = y, width = width, height = height}
end)