local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local collision = require "necro.game.tile.Collision"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local spell = require "necro.game.spell.Spell"
local health = require "necro.game.character.Health"
local confusion = require "necro.game.character.Confusion"
local lineOfSight = require "necro.game.system.LineOfSight"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
	spellcastConfuseEnemies = {
		field.int8("duration",9),
	},
}

event.swipe.add("bullet", "bullet", function (ev)
    ev.entity.swipe.texture = "mods/Touhou/gfx/swipe_bullet.png"
    ev.entity.swipe.frameCount = 7
    ev.entity.swipe.width = 24
    ev.entity.swipe.height = 24
    ev.entity.swipe.duration = 140
end)


-- reisen
customEntities.extend {
    name = "Reisen",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Reisen",
			initialEquipment = {
				items = {
					"Touhou_WeaponGun",
					"ShovelBasic",
					"Bomb",
					"Touhou_SpellLunatic",
					"Touhou_SpellBulletStorm",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_reisen_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_reisen.png",
				focusX = 200,
				focusY = 0,
			},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_reisen_head.png",
			},
		}
	}
}

-- gun
customEntities.extend {
    name = "WeaponGun",
    template = customEntities.template.item("weapon_rifle"),

    data = {
        flyaway = "Gun",
        hint = "Shoot enemies!",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/gun.png",
			width = 26,
			height = 26,
		},
        weapon = {
			damage = 1,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "dagger",
                passWalls = true,
				multihit = true,
                tiles = {
					{
                        offset = {1, 0},
                        damageMultiplier = 0,
                    },
                },
            },
        },
		weaponReloadable = {
            pattern = {
                passWalls = false,
                multiHit = true,
                forceAttack = false,
                repeatTiles = 99,
                tiles = {
                    {
                        offset = {1, 0},
                        attackFlags = attack.mask(attack.Flag.PROVOKE, attack.Flag.DIRECT),
                        damageMultiplier = 3,
						dashDirection = false,
                    },
                    damageMultiplier = 3,
                },
				dashDirection = false,
            },
			maximumAmmo = 8,
			ammoPerReload = 1,
        },
		weaponKnockback = {
			distance = 1,
		},
    },
}



-- bullet storm
customEntities.extend {
    name = "SpellBulletStorm",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Bullet storm",
        hint = "Shoots enemies!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_bullet.png",
        },
        spell = {
            castType = "Touhou_spellcastBullet",
        },
        spellCooldownTime = {
            cooldown = 20,
        },
		spellUseCasterFacingDirection = {},
    }
}
	
-- bullet storm spell
commonSpell.registerSpell("spellcastBullet", {
		spellcastSwipe = {
			coreTexture = "mods/Touhou/gfx/swipe_empty.png",
			textures = {
				"mods/Touhou/gfx/swipe_empty.png",
				"mods/Touhou/gfx/swipe_b1.png",
				"mods/Touhou/gfx/swipe_b2.png",
				"mods/Touhou/gfx/swipe_b3.png",
				"mods/Touhou/gfx/swipe_b4.png",
				"mods/Touhou/gfx/swipe_b5.png",
				"mods/Touhou/gfx/swipe_b6.png",
				"mods/Touhou/gfx/swipe_b7.png",
				"mods/Touhou/gfx/swipe_b8.png",
				"mods/Touhou/gfx/swipe_b9.png",
				"mods/Touhou/gfx/swipe_ba.png",
			},
			width = 24,
            height = 24,
            frameCount = 21,
            duration = 140,
			diagonalScale = math.sqrt(2),
		},
		spellcastMultiTileSwipe = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Bullet",
        },
        soundSpellcast = {
            sound = "spellGeneral",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
		spellcastMulticast = {
			castCount = 8,
			rotation = 2,
		},
    })
	
-- lunatic eyes
customEntities.extend {
    name = "SpellLunatic",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Lunatic Eyes",
        hint = "Confuse enemies!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_lunatic.png",
        },
        spell = {
            castType = "Touhou_spellcastLunatic",
        },
        spellCooldownTime = {
            cooldown = 24,
        },
    }
}
	
-- lunatic spell
commonSpell.registerSpell("spellcastLunatic", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_eyes.png",
			textures = {
				"mods/Touhou/gfx/swipe_eyes.png"
			},
            width = 24,
            height = 24,
            frameCount = 8,
            duration = 400,
        },
		spellcastRectangular = {
			offsetX = -2,
			offsetY = -2,
			width = 5,
			height = 5,
		},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        friendlyName = {
            name = "Lunatic Eyes",
        },
		Touhou_spellcastConfuseEnemies = {},
    })
	
-- confuse
event.spellcast.add(
    "confuseEnemies",
    {
        order = "inflictConfusion",
		filter = "Touhou_spellcastConfuseEnemies",
    },
    function (ev)
		for victim in spellTargeting.entityIterator(ev) do
			if attack.isAttackable(ev.caster, victim) then
				confusion.inflict(victim, ev.entity.Touhou_spellcastConfuseEnemies.duration)
			end
		end
    end
)