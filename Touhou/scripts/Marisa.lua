local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local collision = require "necro.game.tile.Collision"
local action = require "necro.game.system.Action"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local team = require "necro.game.character.Team"
local character = require "necro.game.character.Character"
local inventory = require "necro.game.item.Inventory"
local damage = require "necro.game.system.Damage"
local move = require "necro.game.system.Move"

-- marisa
customEntities.extend {
    name = "Marisa",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Marisa",
			initialEquipment = {
				items = {
					"WeaponEli",
					"ShovelBasic",
					"Touhou_SpellSpark",
					"Touhou_SpellStar",
				},
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_marisa_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_marisa.png",
				focusX = 220,
				focusY = 100,
			},
		}, {
			sprite = {
				texture = "mods/Touhou/gfx/char_marisa_head.png",
			},
		}
	}
}

-- spark
customEntities.extend {
        name = "SpellSpark",
		template = customEntities.template.item(),
		
		data = {
			flyaway = "Master Spark",
			hint = "Shoots the Master Spark!",
			slot = "spell",
		},
		
		components = {
			isSpell = true,
			sprite = {
				texture = "mods/Touhou/gfx/spell_spark.png",
			},
			spell = {
				castType = "Touhou_spellcastSpark",
			},
			spellCooldownTime = {
				cooldown = 64,
			},
			spellUseCasterFacingDirection = {},
		},
}

-- fan spell
commonSpell.registerSpell("spellcastSpark", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_spark.png",
			textures = {
				"mods/Touhou/gfx/swipe_spark.png"
			},
            width = 144,
            height = 216,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
        spellcastCone = {
            distance = 5,
        },
        spellcastAutoDespawn = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        team = {},
        friendlyName = {
            name = "Master Spark",
        },
		spellcastInflictDamage = {
			damage = 8,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PHASING,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
    })

-- star
customEntities.extend {
    name = "SpellStar",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Starburst",
        hint = "Shoots a burst of stars!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_star.png",
        },
        spell = {
            castType = "Touhou_spellcastStar",
        },
        spellCooldownTime = {
            cooldown = 2,
        },
		spellUseCasterFacingDirection = {},
    }
}

-- throw spell
commonSpell.registerSpell("spellcastStar", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_starburst.png",
			textures = {
				"mods/Touhou/gfx/swipe_starburst.png"
			},
            width = 48,
            height = 72,
            frameCount = 8,
            duration = 400,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
        spellcastCone = {
            distance = 2,
        },
        spellcastAutoDespawn = {},
        soundSpellcast = {
            sound = "spellGeneral",
        },
        team = {},
        friendlyName = {
            name = "Starburst",
        },
		spellcastInflictDamage = {
			damage = 3,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.PHASING,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		},
    })