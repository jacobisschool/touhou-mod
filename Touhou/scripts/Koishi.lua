local customEntities = require "necro.game.data.CustomEntities"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local event = require "necro.event.Event"
local object = require "necro.game.object.Object"
local collision = require "necro.game.tile.Collision"
local team = require "necro.game.character.Team"
local attack = require "necro.game.character.Attack"
local damage = require "necro.game.system.Damage"
local dig = require "necro.game.tile.Dig"
local commonWeapon = require "necro.game.data.item.weapon.CommonWeapon"
local move = require "necro.game.system.Move"
local action = require "necro.game.system.Action"
local objectEvents = require "necro.game.object.ObjectEvents"
local player = require "necro.game.character.Player"
local freeze = require "necro.game.character.Freeze"
local inventory = require "necro.game.item.Inventory"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local spell = require "necro.game.spell.Spell"
local health = require "necro.game.character.Health"

local components = require "necro.game.data.Components"
local field = components.field
local constant = components.constant

components.register {
	spellcastHealCaster = {
		constant.int8("amount",1),
	},
	spellcastMassiveKnockback = {},
	immuneToMassiveKnockback = {},
}


-- koishi
customEntities.extend {
    name = "Koishi",
    template = customEntities.template.player(0),
	
    components = {
		{
			FriendlyName = "Koishi",
			initialEquipment = {
				items = {
					"Touhou_WeaponThirdEye",
					"ShovelBasic",
					"Bomb",
					"Touhou_SpellHeartBeam",
					"Touhou_SpellRose",
				},
			},
			health = {
				maxHealth = 10,
				health = 10,
			},
			sprite = {
				texture = "mods/Touhou/gfx/char_koishi_body.png",
			},
			bestiary = {
				image = "mods/Touhou/gfx/bestiary_koishi.png",
				focusX = 175,
				focusY = 0,
			},
			Touhou_immuneToMassiveKnockback = {},
		},
		{
			sprite = {
				texture = "mods/Touhou/gfx/char_koishi_head.png",
			},
		}
	}
}

-- third eye
customEntities.extend {
    name = "WeaponThirdEye",
    template = customEntities.template.item(),

    data = {
        flyaway = "Third Eye",
        hint = "Confuse enemies!",
        slot = "weapon",
    },

    components = {
        sprite = {
            texture = "mods/Touhou/gfx/thirdeye.png",
			width = 26,
			height = 27,
		},
        weapon = {
			damage = 0,
        },
        weaponPattern = {
			pattern = commonWeapon.pattern {
				swipe = "dagger",
                passWalls = false,
                tiles = {
					{
                        offset = {1, 0},
                    },
                },
            },
        },
		weaponKnockback = {
			distance = 1,
		},
		weaponInflictConfusion = {
			duration = 8,
		},

    },
}

-- telephone
customEntities.extend {
    name = "SpellHeartBeam",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Heartrate Beam",
        hint = "Shoots a powerful beam!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_heart.png",
        },
        spell = {
            castType = "Touhou_spellcastHeartBeam",
        },
        spellCooldownTime = {
            cooldown = 3,
        },
		spellUseCasterFacingDirection = {},
    }
}

-- throw spell
commonSpell.registerSpell("spellcastHeartBeam", {
		spellcastSwipe = {
			coreTexture = "mods/Touhou/gfx/swipe_empty.png",
			textures = {
				"mods/Touhou/gfx/swipe_t1.png",
				"mods/Touhou/gfx/swipe_t2.png",
				"mods/Touhou/gfx/swipe_t3.png",
				"mods/Touhou/gfx/swipe_t4.png",
			},
			width = 24,
            height = 24,
            frameCount = 7,
            duration = 400,
			diagonalScale = math.sqrt(2),
		},
		spellcastMultiTileSwipe = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Heartrate Beam",
        },
        soundSpellcast = {
            sound = "spellGeneral",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
		},
		spellcastInflictDamage = {
			damage = 2,
			type = damage.Type.MAGIC,
			penetration = damage.Penetration.NONE,
		},
		spellcastDirectionalOffset = {
			distance = 1,
		}
    })

-- rose
customEntities.extend {
    name = "SpellRose",
	template = customEntities.template.item(),
		
	data = {
        flyaway = "Rose",
        hint = "Shoots a powerful rose!",
        slot = "spell",
    }, 
	
	components = {
        isSpell = true,
        sprite = {
            texture = "mods/Touhou/gfx/spell_rose.png",
        },
        spell = {
            castType = "Touhou_spellcastRose",
        },
        spellCooldownTime = {
            cooldown = 24,
        },
		spellUseCasterFacingDirection = {},
    }
}

-- rose spell
commonSpell.registerSpell("spellcastRose", {
		spellcastSwipe = {
			textures = {
				"mods/Touhou/gfx/swipe_empty.png",
				"mods/Touhou/gfx/swipe_rose.png",
			},
			width = 24,
            height = 24,
            frameCount = 3,
            duration = 350,
			diagonalScale = math.sqrt(2),
		},
		spellcastMultiTileSwipe = {},
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
        team = {},
        friendlyName = {
            name = "Rose",
        },
        soundSpellcast = {
            sound = "spellGeneral",
        },
		spellcastLinear = {
			collisionMask = collision.mask(collision.Type.WALL),
		},
		spellcastInflictDamage = {
			damage = 3,
			type = damage.Type.PHYSICAL,
			penetration = damage.Penetration.NONE,
		},
		Touhou_spellcastHealCaster = {
			amount = 1,
		},
		Touhou_spellcastMassiveKnockback = {},
    })
	
-- flower
commonSpell.registerSpell("spellcastFlower", {
		spellcastSwipe = {
            coreTexture = "mods/Touhou/gfx/swipe_flower.png",
			textures = {
				"mods/Touhou/gfx/swipe_flower.png",
			},
            width = 24,
            height = 24,
            frameCount = 8,
            duration = 350,
            diagonalScale = math.sqrt(2),
        },
		spellcastProcessImmediately = {},
        spellcastAutoDespawn = {},
    })

-- heal
event.spellcast.add(
    "HealCaster",
    {
        order = "offset",
		sequence = 1,
		filter = "Touhou_spellcastHealCaster",
    },
    function (ev)
		-- ev.caster.health.health = ev.caster.health.health + ev.entity.Touhou_spellcastHealCaster.amount
		-- if ev.caster.health.health > ev.caster.health.maxHealth then
			-- ev.caster.health.health = ev.caster.health.maxHealth
		-- end
    end
)

-- massive knockback
event.spellcast.add(
    "massiveKnockback",
    {
        order = "offset",
		filter = "spellcastInflictDamage",
    },
    function (ev)
		if ev.entity:hasComponent("Touhou_spellcastMassiveKnockback") then
			for victim in spellTargeting.entityIterator(ev) do
				if victim:hasComponent("team") then
					if attack.isAttackable(ev.caster, victim) then
						for i = 0,100,1 do
							move.direction(victim, ev.caster.facingDirection.direction, 1, move.Type.KNOCKBACK)
						end
						if victim:hasComponent("beatDelay") then
							victim.beatDelay.counter = victim.beatDelay.counter + 1
						end
						if victim:hasComponent("health") and ev.entity:hasComponent("Touhou_spellcastHealCaster") then
							spell.cast(victim,"Touhou_spellcastFlower", ev.caster.facingDirection.direction)
							if victim.health.health - ev.entity.spellcastInflictDamage.damage > 0 then
								ev.caster.health.health = ev.caster.health.health + ev.entity.Touhou_spellcastHealCaster.amount
								if ev.caster.health.health > ev.caster.health.maxHealth then
									ev.caster.health.health = ev.caster.health.maxHealth
								end
							end
						end
					end
				end
			end
		end
    end
)